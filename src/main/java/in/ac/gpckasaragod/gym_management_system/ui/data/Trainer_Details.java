/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.gym_management_system.ui.data;

import java.sql.Date;

/**
 *
 * @author student
 */
public class Trainer_Details {
    private Integer id;
    private String trainerName;
    private String address;
    private Integer age;
    private String mobile;
    private Date dateOfJoin;

