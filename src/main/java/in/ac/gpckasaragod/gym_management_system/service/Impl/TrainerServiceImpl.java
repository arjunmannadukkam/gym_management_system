/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.gym_management_system.service.Impl;

import com.mysql.cj.protocol.Resultset;
import in.ac.gpckasaragod.gym_management_system.service.Trainer;
import in.ac.gpckasaragod.gym_management_system.ui.TrainerDetails;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class TrainerServiceImpl extends ConnectionServiceImpl implements Trainer{

    @Override
    public String saveTrainer(String trainerName, Integer age, String address, Integer mobile, Date dateOfJoin) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
           try{
    Connection connection = getConnection();
    Statement statement = connection.createStatement();
               String name = null;
    String query ="INSERT INTO TRAINER_DETAILS(ID,TRAINER_NAME,AGE,ADDRESS,MOBILE,DATE_OF_JOIN) VALUES " +"('"+trainerName+"','"+age+"','"+address+"','"+mobile+"','"+dateOfJoin+"')";
    System.out.println("Query:"+query);
    int status = statement.executeUpdate(query);
    if(status !=1) {
        return "save failed";
    }else {
        return "saved successfully";
    }

        } catch (SQLException ex) {
            Logger.getLogger(TrainerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return"save failed";
    }

    @Override
    public TrainerDetails readTrainerDetails(Integer Id) {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       TrainerDetails trainerDetails =null;
     try{
        Connection connection =getConnection();
        Statement statement = connection.createStatement();
        
        String query = "SELECT * FROM TRAINER WHERE ID="+Id;
        ResultSet resultSet = statement.executeQuery(query);
        while(resultSet.next()){
            int id = resultSet .getInt("ID");
            String name = resultSet .getString("TRAINER_NAME");
            Integer age = resultSet .getInt("AGE");
            String address = resultSet .getString("ADDRESS");
            Integer mobile =resultSet.getInt("MOBILE");
            Date dateOfJoin = resultSet .getDate("DATE_OF_JOIN");
            TrainerDetails trainerdetails = new TrainerDetails(trainerName,age,address,mobile,dateOfJoin);
                    }
    

     }  catch (SQLException ex) {
            Logger.getLogger(TrainerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return trainerDetails;
    }

    /**
     *
     * @return
     */
    @Override
     public List<TrainerDetails>getALLTrainerDetails() {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       List<TrainerDetails> trainerDetailss = new ArrayList<>();
      try{
          Connection connection =getConnection();
          Statement statement = connection.createStatement();
          String query = "SELECT * FROM TRAINER_DETAILS";
        ResultSet resultSet = statement.executeQuery(query);
          
          while(resultSet.next()) {
              int id = resultSet.getInt("ID");
              
            String name = resultSet.getString("TRAINER_NAME");
            Integer age = resultSet .getInt("AGE");
            String address = resultSet .getString("ADDRESS");
            String mobile =resultSet .getString("MOBILE");
            Date dateOfJoin = resultSet .getDate("DATE_OF_JOIN");
            TrainerDetails trainerDetails  = new TrainerDetails(name,age,address,mobile,dateOfJoin);
              trainerDetailss.add(trainerDetails);
              
          }
      } catch (SQLException ex) {
            Logger.getLogger(TrainerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return trainerDetailss;

    
    }

   

    @Override
    public String updateTrainerDetails(Integer id, String name,Integer age, Integer mobile, String address) throws SQLException {
       try { 
    //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }Connection connection = getConnection();
            Statement statement = connection.createStatement();
        String trainerName = null;
            String query = "UPDATE TRAINER_DETAILS SET TRAINER_NAME='"+trainerName+"',AGE='"+age+"',MOBILE='"+mobile+"',ADDRESS='"+address+"',DATE_OF_JOIN='"+dateOfJoin+"' WHERE ID="+id;
                    
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
                return "Update failed";
            else
                return "Update successfully";
            }catch(SQLException ex){
                //Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            }
                return "Update failed";
                

    public String deleteTrainerDetails(Integer id) {
       try { 
    //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  