/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.gym_management_system.service;

import in.ac.gpckasaragod.gym_management_system.ui.TrainerDetails;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author student
 */
public interface Trainer {
    public String saveTrainerDetails(String trainerName,Integer age,String address,Integer mobile,Date dateOfJoin);
    public TrainerDetails readTrainerDetails(Integer Id);
    public List<TrainerDetails>getALLTrainerDetails();
    public String updateTrainerDetails(Integer id,String trainerName,Integer age,Integer mobile,String address,Date dateOfJoin);
    public String deleteTrainerDetails(Integer id);
}
